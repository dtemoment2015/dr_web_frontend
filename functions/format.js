export function xml2json(xmlData) {

    var parser = require('fast-xml-parser');
    var he = require('he');

    var options = {
        attributeNamePrefix: "@_",
        attrNodeName: "attr", //default is 'false'
        textNodeName: "#text",
        ignoreAttributes: true,
        ignoreNameSpace: false,
        allowBooleanAttributes: false,
        parseNodeValue: true,
        parseAttributeValue: false,
        trimValues: true,
        cdataTagName: "__cdata", //default is 'false'
        cdataPositionChar: "\\c",
        parseTrueNumberOnly: false,
        arrayMode: false, //"strict"
        attrValueProcessor: (val, attrName) => he.decode(val, { isAttributeValue: true }),//default is a=>a
        tagValueProcessor: (val, tagName) => he.decode(val), //default is a=>a
        stopNodes: ["parse-me-as-string"]
    };

    if (parser.validate(xmlData) !== true) { //optional (it'll return an object in case it's not valid)
        return {};
    }

    var tObj = parser.getTraversalObj(xmlData, options);

    return parser.convertToJson(tObj, options);

};

export function cartSession() {
    let s4 = () => {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

export function catchNotify(vToastify, data) {
    if (data.errors) {
       setTimeout(() => {
          let k = 0;
          Object.keys(data.errors).forEach((item) => {
             let err = data.errors[item];
             if (k > 2) {
                return false;
             }
             k++;
             for (let i = 0; i < err.length; i++) {
                setTimeout(() => {
                   vToastify.error(err[i], "Ошибка")
                }, 100 * k);
             }
           
          });
       }, 600);
    }
    else if (data.message) {
       vToastify.error(data.message, "Ошибка")
    }
 };