import Vue from 'vue';
import VueToastify from "vue-toastify";
Vue.use(VueToastify, {

   customNotifications: {
      clientSuccess: {
         body: "You did it!",
         defaultTitle: false,
         icon: '',
         canTimeout: false
      }
   },

   hideProgressbar: true,
   position: 'top-left'

}
);