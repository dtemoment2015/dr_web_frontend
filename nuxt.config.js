export default {
  server: {
    port: 3003, // default: 3000
    host: '127.0.0.1', // default: localhost
  },

  mode: 'universal',

  target: 'server',

  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  css: [
    {
      src: '~assets/scss/index.scss',
      lang: 'scss'
    },
  ],
  plugins: [
    { src: '~/plugins/filters.js', ssr: true },
    { src: '~/plugins/toastr.js', mode: 'client' },
    { src: '~/plugins/mask.js', mode: 'client'  },
    { src: '~/plugins/axios.js', ssr: true },
  ],
  components: true,
  buildModules: [
  ],

  modules: [
    'cookie-universal-nuxt',
    '@nuxtjs/axios',
  ],

  serverMiddleware: [
    { path: '/basket', handler: '~/middleware/postRequestHandler.js' },
  ],

  build: {
  },

  axios: {
    baseURL: 'https://api2.rrpo.uz/api/',
    // baseURL: 'http://127.0.0.1:8002/api/',
    withCredentials: true
  },
}