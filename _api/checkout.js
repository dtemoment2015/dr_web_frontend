
import { catchNotify } from '~/functions/format'

export function orderCheckout(user = null) {
   let id = this.$vToastify.loader("Подождите, идет процесс", "Оформление");
   let data = this.$store.state.cart.product;
   this.$store.dispatch('setBlock', true);
   return this.$axios({
      method: 'post',
      url: 'checkout',
      data: {
         // title: data.title.ru,
         cart_hash: data.cart_hash,
         session: data.session,
         user: user,
         // remotekeys: data.remotekeys,
         // session: data.session,
         // licence: data.license.item,
         // subscription_product_name: data.subscription_product_name,
         // price: data.price,
         // preset_client_type: data.preset_client_type,
         // quickorder: data.quickorder,
         // quickoidrder: data.id,
         // period: data.period,
      }
   }).then(res => {
      if (res.data) {
         this.$router.push({ path: `/order/${res.data.hash}` });
         setTimeout(() => {
            this.$store.dispatch('setCart', null);
            this.$store.dispatch('setSession', null);
            this.$cookies.removeAll();
         }, 200);
      }
   }).catch(error => {
      if (error.response) {
         catchNotify(this.$vToastify, error.response.data);
      }
   }).finally(() => {
      this.$vToastify.stopLoader(id);
      this.$store.dispatch('setBlock', false);
   });

};