const order = (axios, hash) => {

   return axios({
      method: 'post',
      url: `check/complete`,
      params:{
         hash:hash
      }
   }).then(res => {
      return res.data.xml;
   }).catch(() => {
      return null
   })
}

export default order