const order = (axios, hash) => {

   return axios({
      method: 'get',
      url: `order`,
      params:{
         hash:hash
      }
   }).then(res => {
      return res.data;
   }).catch(() => {
      return []
   })
}

export default order