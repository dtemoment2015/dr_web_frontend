import { cartSession } from "~/functions/format";

export const state = () => ({
   cartSession: null,
   cart: null,
   blockButton: false
})

export const mutations = {
   SET_SESSION(state, payload) {
      state.cartSession = payload;
   },
   SET_CART(state, payload) {
      state.cart = payload;
   },
   SET_BLOCK(state, payload) {
      state.blockButton = payload;
   }
}

export const actions = {
   setSession({ commit }, cartSession) {
      commit('SET_SESSION', cartSession);
   },
   setCart({ commit }, cart) {
      commit('SET_CART', cart)
   },
   setBlock({ commit }, cart) {
      commit('SET_BLOCK', cart)
   },
   async nuxtServerInit({ commit, dispatch, state }, { app, $axios, store }) {
      let cSession = cartSession();
      if (app.$cookies.get('cartSession')) {
         cSession = app.$cookies.get('cartSession');
      } else {
         app.$cookies.set('cartSession', cSession, {
            maxAge: 31536000
         });
      }
      dispatch('setSession', cSession);

   }

}